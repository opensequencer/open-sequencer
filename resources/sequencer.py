import math

import schemas.sequencer as sq
from schemas.sequencer import ACTION_PROPERTIES


def set_default_values(raw_message):
    """
    Define valores padrões para a mensagem
    """
    clean_message = {}

    for action in ACTION_PROPERTIES:
        if not has_action(raw_message, action):
            clean_message[action] = ACTION_PROPERTIES[action]["max"]
        else:
            clean_message[action] = [
                message["value"]
                for message in raw_message
                if message["action"] == action
            ][0]
    return clean_message


def has_action(raw_message, action):
    """
    Checa se a mensagem contém uma nota
    """
    return any([action in message["action"] for message in raw_message])


def apply_mod(sequencers):
    """
    Pega os sequenciadores com actions iguais e aplica um MOD neles
    """
    (
        unique_sequencers,
        repeated_sequencers,
        repeated_actions,
    ) = sq.split_sequencers_by_parity(sequencers)

    if repeated_sequencers:
        repeated_sequencers = sq.mod_repeated_values(
            repeated_sequencers, repeated_actions
        )
        return repeated_sequencers + unique_sequencers
    return sequencers


def apply_octave(raw_message):
    """
    Força a nota dentro da oitava definida
    """

    octaved_message = []
    octave_sequencer = next(
        (sequencer for sequencer in raw_message if sequencer["action"] == "Octave"),
        None,
    )
    if octave_sequencer:  # retirar o else
        note_sequencer = next(
            (sequencer for sequencer in raw_message if sequencer["action"] == "Note"),
            None,
        )
        octaved_message.append(
            {
                "action": "Note",
                "value": force_octave(
                    note_sequencer["value"], octave_sequencer["value"]
                ),
            }
        )
        not_octave_or_note = [
            sequencer
            for sequencer in raw_message
            if (sequencer["action"] not in ["Octave", "Note"])
        ]
        octaved_message = octaved_message + not_octave_or_note
    else:
        octaved_message = raw_message
    return octaved_message


def get_actual_values(sequencers, tick_value, tick_counter):
    """
    Pega os valores setados nos sequenciadores
    """

    raw_message = []
    for sequencer in sequencers:
        if sequencer.status:
            raw_position = (tick_value / sequencer.bar_tempo_rate) * (tick_counter - 1)
            fader_tick_position = math.floor(raw_position) % 16
            message = sq.filter_repeated_notes(
                raw_position, sequencer, fader_tick_position
            )
            raw_message.append(message)
    return raw_message


def get_first_note_sequencer(sequencers):
    """
    Pega os sequencers por actions
    """
    note_sequencer = next(
        (sequencer for sequencer in sequencers if sequencer.action == "Note"), None,
    )
    return note_sequencer


def get_sequencers_channels(controller):
    """
    Pega os canais dos sequencers
    """
    channels = {sequencer.channel for sequencer in controller.sequencers}
    return channels


def sort_sequencers_by_channel(controller):
    """
    Ordena os sequencers por canal e retorna
    """
    sequencers_by_channel = []
    channels = get_sequencers_channels(controller)
    for channel in channels:
        sequencers = [
            sequencer
            for sequencer in controller.sequencers
            if sequencer.channel == channel
        ]
        sequencers_by_channel.append({"channel": channel, "sequencers": sequencers})
    return sequencers_by_channel


def force_octave(note, octave):
    """
    Função que converte a nota absoluta para oitava
    WIP
    """
    # Se for uma nota invalidada apenas replica
    if note < 0:
        octaved_note = note
    else:
        octaved_note = math.floor((note / 128) * 12 + (12 * octave))
    return octaved_note
