import threading

from modules.message import build_message


def start_worker(controller):
    """
    Função recursiva que executa threads em um intervalo de tempo definido
    """

    def worker():
        """
        Warpper da Função a ser executada
        """
        start_worker(controller)
        execute_actions(controller)

    new_thread = threading.Timer(get_tick_time(controller), worker)
    new_thread.start()
    controller.worker = new_thread


def execute_actions(controller):
    """
    Incrementa o Tick e envia mensagens
    """
    count_tick(controller)
    tick_value = get_tick_value(controller)
    message = build_message(controller, tick_value, controller.tick_counter,)
    controller.update_tick_in_faders(tick_value, controller.tick_counter)
    controller.interface_midi.trigger_message(message)


def count_tick(controller):
    """
    Função que faz a contagem do tick
    """
    controller.tick_counter += 1
    if controller.tick_counter > ((1 / get_tick_value(controller)) * 16):
        controller.tick_counter = 1


def get_tick_value(controller):
    """
    Função que identifica o menor valor de bar_tempo_rate nos sequenciadores
    """
    return min([sequencer.bar_tempo_rate for sequencer in controller.sequencers])


def get_tick_time(controller):
    """
    Funçao que retorna o valor do tick em milisegundos
    """
    return (60 / controller.bpm) * get_tick_value(controller)
