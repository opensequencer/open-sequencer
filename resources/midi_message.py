from mido import Message


def build_midi_message(clean_message, channel):
    """
    Transforma os valores em NoteOn, Pause e NoteOff
    """

    note_on = Message(
        "note_on",
        note=clean_message["Note"],
        velocity=clean_message["Velocity"],
        channel=channel,
    )
    note_off = Message("note_off", note=clean_message["Note"], channel=channel)

    return note_on, clean_message["Duration"], note_off
