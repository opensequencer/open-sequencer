import math

from kivy.animation import Animation
from kivy.clock import mainthread
from kivy.properties import ObjectProperty  # pylint:disable=no-name-in-module
from kivy.uix.button import Button
from kivy.uix.slider import Slider
from kivy.uix.widget import Widget

import views.shared as shared_variables
from modules.sequencer import new_sequencer
from schemas.sequencer import ACTION_PROPERTIES

BAR_TEMPO_RATES = ["1", "1/2", "1/4", "1/8", "1/16", "1/32"]


def float_to_fraction(float_value):
    """
    Converte float em uma string da fração
    """
    if isinstance(float_value, float):
        fraction = (
            str(float_value.as_integer_ratio()[0])
            + "/"
            + str(float_value.as_integer_ratio()[1])
        )
    else:
        fraction = str(float_value)
    return fraction


class SequencerColumn(Widget):
    toggle_button = ObjectProperty(None)
    sequencer_column_grid = ObjectProperty(None)
    sequencers_box = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(SequencerColumn, self).__init__(**kwargs)
        self.load_sequencers()

    @mainthread
    def load_sequencers(self):
        """
        Carrega os sequenciadores do controller
        """
        for i in range(0, len(shared_variables.CONTROLLER.sequencers)):
            created_sequencer_button = Button(
                size_hint_x=None,
                text="Sequencer-" + str(i),
                on_press=self.set_sequencer,
            )
            self.sequencers_box.add_widget(created_sequencer_button)

    def toggle(self):
        """
        Opens and closes lateral tab
        WIP
        """
        rectangle = self.canvas.get_group("rectangle")[0]
        if rectangle.size[0] < (self.width / 10):
            anim_rect = Animation(size=(self.width / 6, self.height), duration=0.1)
            anim_rect.start(rectangle)
            anim_toogle = Animation(x=self.x / 6, duration=0.1)
            anim_toogle.start(self.sequencer_column_grid)
        else:
            anim_rect = Animation(size=(self.width / 12, self.height), duration=0.1)
            anim_rect.start(rectangle)
            anim_toogle = Animation(x=-self.width / 12, duration=0.1)
            anim_toogle.start(self.sequencer_column_grid)

    def add_sequencer(self):
        """
        Adiciona um novo sequenciador no controller e na interface
        """
        sequencers = shared_variables.CONTROLLER.sequencers
        created_sequencer = new_sequencer(
            action="Note",
            action_min=0,
            action_max=127,
            bar_tempo_rate=1,
            index=len(sequencers),
            status=False,
            channel=0,
        )
        created_sequencer_button = Button(
            size_hint_x=None,
            text="Sequencer-" + str(created_sequencer.index),
            on_press=self.set_sequencer,
        )
        self.sequencers_box.add_widget(created_sequencer_button)

    def set_sequencer(self, sequencer_button):
        """
        Seta o sequenciador selecionado na tela
        """
        index = int(sequencer_button.text.split("-")[1])
        shared_variables.ACTUAL_SEQUENCER = shared_variables.CONTROLLER.sequencers[
            index
        ]
        self.parent.sequencer_faders.update_sequencer()
        self.parent.sequencer_controls.load_sequencer()
        shared_variables.CONTROLLER.update_tick_in_faders(clear_tick=1)

    def delete_sequencer(self):
        """
        Deleta o sequenciador
        """


class SequencerControls(Widget):
    action_properties = ACTION_PROPERTIES
    bar_tempo_rates = BAR_TEMPO_RATES
    sequencer = ""
    action = ObjectProperty(None)
    action_range = ObjectProperty(None)
    bar_tempo_rate = ObjectProperty(None)
    channel = ObjectProperty(None)
    sequencer_status = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(SequencerControls, self).__init__(**kwargs)
        self.load_sequencer()

    @mainthread
    def load_sequencer(self):
        """
        Instancia o sequenciador selecionado
        """
        self.sequencer = shared_variables.ACTUAL_SEQUENCER
        self.action.text = self.sequencer.action
        self.action_range.value1 = self.sequencer.action_min
        self.action_range.value2 = self.sequencer.action_max
        self.bar_tempo_rate.text = float_to_fraction(self.sequencer.bar_tempo_rate)
        self.channel.text = str(self.sequencer.channel + 1)
        self.sequencer_status.text = "ON" if self.sequencer.status else "OFF"

    def on_action_change(self, action):
        """
        Função de alteração da action
        """
        self.sequencer.action = action
        self.sequencer.action_min = ACTION_PROPERTIES[action]["min"]
        self.action_range.value1 = ACTION_PROPERTIES[action]["min"]
        self.action_range.min = ACTION_PROPERTIES[action]["min"]
        self.sequencer.action_max = ACTION_PROPERTIES[action]["max"]
        self.action_range.max = ACTION_PROPERTIES[action]["max"]
        self.action_range.value2 = ACTION_PROPERTIES[action]["max"]
        self.sequencer.update_absolute_values()

    def on_bar_tempo_change(self, bar_tempo_rate):
        """
        Função de alteração do bartempo
        """
        if bar_tempo_rate != "1":
            partial_tempo = bar_tempo_rate.split("/")
            partial_tempo = int(partial_tempo[0]) / int(partial_tempo[1])
            self.sequencer.bar_tempo_rate = partial_tempo
        else:
            self.sequencer.bar_tempo_rate = int(bar_tempo_rate)
        shared_variables.CONTROLLER.update_tick_in_faders(clear_tick=1)

    def on_channel_change(self, channel):
        """
        Função de alteração do channel.
        Obs:
        Mido trabalha com valores entre 0 e 15
        e o padrão do midi é 1 a 16
        """
        self.sequencer.channel = int(channel) - 1

    def on_range_change(self, min_val, max_val):
        self.sequencer.action_min = int(min_val)
        self.sequencer.action_max = int(max_val)
        self.sequencer.update_absolute_values()

    def on_status_change(self, status_button):
        """
        Função de alteração do status
        """
        if self.sequencer.status:
            self.sequencer.status = False
            status_button.text = "OFF"
        else:
            self.sequencer.status = True
            status_button.text = "ON"


class SequencerFaders(Widget):

    title = ObjectProperty()
    faders_box = ObjectProperty()
    sequencer = ""
    action_properties = ACTION_PROPERTIES
    inverted_index = list(reversed(range(0, 16)))
    value_default_color = [230 / 255, 42 / 255, 63 / 255, 1]
    value_selected_color = [230 / 255, 228 / 255, 1 / 255, 1]

    def __init__(self, **kwargs):
        super(SequencerFaders, self).__init__(**kwargs)
        self.initialize_faders()

    @mainthread
    def initialize_faders(self):
        """
        Instancia o sequenciador selecionado
        """
        self.sequencer = shared_variables.ACTUAL_SEQUENCER
        self.make_faders()
        shared_variables.CONTROLLER.update_tick_in_faders = self.update_tick_in_faders

    def update_sequencer(self):
        """
        Atualiza os valores dos faders
        """
        self.sequencer = shared_variables.ACTUAL_SEQUENCER
        for count, child in enumerate(self.faders_box.children):
            child.value = self.sequencer.faders_values[self.inverted_index[count]]

    def update_tick_in_faders(self, tick_value=0, tick_counter=0, clear_tick=0):
        """
        Atualiza a posição do tick na interface visual
        """
        faders = self.faders_box.children
        if not clear_tick:
            raw_position = (
                tick_value / shared_variables.ACTUAL_SEQUENCER.bar_tempo_rate
            ) * (tick_counter - 1)

            tick_position = math.floor(raw_position) % 16
            selected_fader = self.inverted_index[tick_position]
            unselected_fader = self.inverted_index[tick_position - 1]
            faders[unselected_fader].value_track_color = self.value_default_color
            faders[selected_fader].value_track_color = self.value_selected_color
        else:
            for fader in faders:
                fader.value_track_color = self.value_default_color

    def make_faders(self):
        """
        Faz os 16 faders
        """
        self.faders_box.clear_widgets()  # ensure that box is empity
        for count, value in enumerate(self.sequencer.faders_values):
            new_slider = Slider(id=str(count), value=value)
            new_slider.bind(value=self.on_slider_change)
            self.faders_box.add_widget(new_slider)

    def on_slider_change(self, this_slider, value):
        """
        Função de alteração do slider
        """
        self.sequencer.faders_values[int(this_slider.id)] = value
        self.sequencer.update_absolute_values()
        #self.debug_values()

    @staticmethod
    def debug_values():
        """
        Função de debug para visualizar os valores dos faders
        """
        for count, sequencer in enumerate(shared_variables.CONTROLLER.sequencers):
            print(
                count,
                sequencer.action,
                sequencer.absolute_values,
                shared_variables.CONTROLLER.bpm,
            )
