from kivy.app import App
from kivy.properties import ObjectProperty  # pylint:disable=no-name-in-module
from kivy.uix.floatlayout import FloatLayout

import views.shared as shared_variables
from modules.controller import Controller
from modules.sequencer import new_sequencer

shared_variables.CONTROLLER = Controller()
shared_variables.ACTUAL_SEQUENCER = new_sequencer(
    action="Note",
    action_min=0,
    action_max=127,
    bar_tempo_rate=1 / 2,
    index=0,
    status=True,
    channel=0,
)


class MainScreen(FloatLayout):
    sequencer_faders = ObjectProperty(None)
    sequencer_controls = ObjectProperty(None)


class OpenSequencer(App):
    def build(self):
        return MainScreen()
