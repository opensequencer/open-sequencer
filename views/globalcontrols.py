from kivy.clock import mainthread
from kivy.uix.label import Label
from kivy.uix.widget import Widget

import views.shared as shared_variables


class BpmLabel(Label):
    bpm_max = 210
    bpm_min = 30

    def on_touch_move(self, touch):
        self.update_bpm_value(touch)

    def update_bpm_value(self, touch):
        """
        Atualiza o valor do bpm no controller baseado no toque na tela
        """
        touch_posy = touch.pos[1]
        bpm_label_posy = self.pos[1]
        bpm_label_relative_pos = bpm_label_posy + (self.height / 2)
        bpm_value = int(self.text)

        if touch_posy > bpm_label_relative_pos:
            if bpm_value < self.bpm_max:
                bpm_value = bpm_value + 1
                self.text = str(bpm_value)
        if touch_posy < bpm_label_relative_pos:
            if bpm_value > self.bpm_min:
                bpm_value = bpm_value - 1
                self.text = str(bpm_value)

        shared_variables.CONTROLLER.bpm = bpm_value


class GlobalControls(Widget):
    def __init__(self, **kwargs):
        super(GlobalControls, self).__init__(**kwargs)
        self.load_controller()

    @mainthread
    def load_controller(self):
        """
        Carrega o controller
        """
        self.controller = shared_variables.CONTROLLER

    def on_pause(self, pause_button):  # pylint: disable=unused-argument
        """
        Dá o pause no controller
        """
        self.controller.pause()
        print("Pausing...")

    def on_play(self, playbutton):  # pylint: disable=unused-argument
        """
        Dá o play no controller
        """
        self.controller.play()
        print("Playing...")

    def on_stop(self, stop_button):  # pylint: disable=unused-argument
        """
        Dá o stop no controller
        """
        self.controller.stop()
        print("Stopping...")
