import threading

from sockets.gpio_interface import GPIOInterface
from sockets.virtual_interface import VirtualInterface


class MidiInteface:
    """
    Faz a Interface com os clientes MIDI
    """

    output_device = "LOCAL"

    def __init__(self):
        self.load_output()
        self.thread_manager = {}

    def load_output(self):
        """
        Carrega o tipo de output selecionado
        """
        if self.output_device == "LOCAL":
            self.output = VirtualInterface()
        if self.output_device == "GPIO":
            self.output = GPIOInterface()

    def stop_note(self, note_off):
        """
        Envia o sinal de note_off
        """
        self.output.send_message(note_off)
        self.thread_manager.pop(note_off.channel, None)

    def play_note(self, note_on, duration, note_off):
        """
        Envia o sinal de note_on e chama uma thread para o note_off
        """
        self.output.send_message(note_on)

        stop_note = threading.Timer(duration, self.stop_note, note_off)
        self.thread_manager[note_on.channel] = stop_note

    def trigger_message(self, messages):
        """
        Prepara e incia a thread que executa as notas
        """
        for message in messages:
            if message[0].note:
                note_on, duration, note_off = message
                channel_note = threading.Thread(
                    target=self.play_note,
                    args=(note_on, duration, note_off),
                    daemon=True,
                )
                if note_on.channel in self.thread_manager:
                    self.thread_manager[note_on.channel].cancel()
                channel_note.start()
