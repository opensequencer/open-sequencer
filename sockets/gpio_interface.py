import queue
import threading

import pigpio
import serial


def set_midi_baudrate(midi_baudrate):
    """
    Seta o o baudrate do gpio em 31250
    """
    serial.Serial(
        port="/dev/ttyAMA0",
        baudrate=midi_baudrate,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1,
    )


class GPIOInterface:
    """
    Faz a Interface com GPIO do Raspberry
    """

    gpio_pin = 16
    MIDI_BAUDRATE = 31250

    def __init__(self):
        self.message_buffer = queue.Queue()
        # set_midi_baudrate(self.MIDI_BAUDRATE)

        self.pi_instance = pigpio.pi()
        self.pi_instance.set_mode(self.gpio_pin, pigpio.OUTPUT)

        self.buffer_manager = threading.Thread(target=self.manage_buffer, daemon=True)
        self.buffer_manager.start()

    def send_message(self, message):
        """
        Adiciona a mensagem em bytes ao buffer para ser enviada
        """
        self.message_buffer.put(message.bytes())

    def manage_buffer(self):
        """
        Administra o buffer de mensagens
        """
        while True:
            if not self.pi_instance.wave_tx_busy():
                message = self.message_buffer.get()
                self.send_to_pin(message)
                self.message_buffer.task_done()

    def send_to_pin(self, message):
        """
        Envia para o pin GPIO
        """
        self.pi_instance.wave_clear()
        self.pi_instance.wave_add_new()
        self.pi_instance.wave_add_serial(self.gpio_pin, self.MIDI_BAUDRATE, message)
        wid = self.pi_instance.wave_create()
        self.pi_instance.wave_send_once(wid)
