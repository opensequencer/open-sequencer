import mido


class VirtualInterface:
    def __init__(self):
        self.outputs = mido.get_output_names()
        self.outport = open_output_connection()

    def send_message(self, message):
        """
        Envia mensagem para interface virtual MIDI
        """
        self.outport.send(message)


def open_output_connection(output_name=None):
    """
    Abre conexão com cliente MIDI
    """
    try:
        return mido.open_output(output_name)
    except IOError as connection_error:
        print(connection_error)
