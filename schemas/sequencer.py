ACTION_PROPERTIES = {
    "Note": {"min": 0, "max": 127},
    "Velocity": {"min": 0, "max": 127},
    "Octave": {"min": 0, "max": 10},
    "Duration": {"min": 20, "max": 2000},
    "Channel": {"min": 1, "max": 16},
}


def split_sequencers_by_parity(sequencers):
    sequencers_actions = [sequencer["action"] for sequencer in sequencers]
    repeated_actions = {
        x for x in sequencers_actions if sequencers_actions.count(x) > 1
    }
    if not repeated_actions:
        return None, None, None
    unique_actions = {x for x in sequencers_actions if sequencers_actions.count(x) == 1}
    unique_sequencers = []
    repeated_sequencers = []
    for action in unique_actions:
        for sequencer in sequencers:
            if sequencer["action"] == action:
                unique_sequencers.append(sequencer)
    for action in repeated_actions:
        for sequencer in sequencers:
            if sequencer["action"] == action:
                repeated_sequencers.append(sequencer)
    return unique_sequencers, repeated_sequencers, repeated_actions


def mod_repeated_values(sequencers, actions, repeated_action_sum=0):
    mod_sequencers = []
    for action in actions:
        for sequencer in sequencers:
            if sequencer["action"] == action:
                repeated_action_sum += sequencer["value"]
        mod_sequencers.append(
            {
                "action": action,
                "value": repeated_action_sum % ACTION_PROPERTIES[action]["max"],
            }
        )
    return mod_sequencers


def filter_repeated_notes(raw_position, sequencer, fader_tick_position):
    if not raw_position.is_integer() and sequencer.action == "Note":
        return {"action": sequencer.action, "value": 0}
    return {
        "action": sequencer.action,
        "value": sequencer.absolute_values[fader_tick_position],
    }
