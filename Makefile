install-dependencies:
	sudo add-apt-repository ppa:kivy-team/kivy
	sudo apt-get install python3-kivy
	pip3 install -r requirements.txt

run:
	python3 app.py
