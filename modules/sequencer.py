import math
import random

from modules.controller import Controller as ct


class Sequencer:  # pylint: disable=too-many-instance-attributes
    def __init__(
        self,
        action=None,
        action_min=None,
        action_max=None,
        bar_tempo_rate=None,
        channel=None,
        index=None,
        status=None,
    ):

        self.faders_values = [random.random() for e in range(16)]
        self.absolute_values = 16 * [0]
        self.action = action
        self.action_min = action_min
        self.action_max = action_max
        self.bar_tempo_rate = bar_tempo_rate
        self.channel = channel
        self.index = index
        self.status = status

        self.update_absolute_values()

    def update_absolute_values(self):
        action_types = {
            "Note": range(self.action_min, self.action_max),
            "Velocity": range(self.action_min, self.action_max),
            "Octave": range(self.action_min, self.action_max),
            "Duration": range(self.action_min, self.action_max),
            "Channel": range(self.action_min, self.action_max),
        }

        interval = len(action_types[self.action])

        for count, fader in enumerate(self.faders_values):
            self.absolute_values[count] = math.floor(interval * fader) + self.action_min


def new_sequencer(**kwargs):
    sequencer = Sequencer(
        action=kwargs["action"],
        action_min=kwargs["action_min"],
        action_max=kwargs["action_max"],
        bar_tempo_rate=kwargs["bar_tempo_rate"],
        channel=kwargs["channel"],
        index=kwargs["index"],
        status=kwargs["status"],
    )
    ct.sequencers.append(sequencer)
    return sequencer
