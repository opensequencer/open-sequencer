from resources.controller import start_worker
from sockets.midi_interface import MidiInteface


class Controller:
    bpm = 120
    sequencers = []
    interface_midi = MidiInteface()
    tick_counter = 0
    status = False

    def __init__(self):
        self.worker = None

    def pause(self):
        if self.worker:
            self.worker.cancel()
            self.status = False

    def play(self):
        # https://kivy.org/doc/stable/api-kivy.uix.button.html
        if not self.status:
            start_worker(self)
            self.status = True

    def stop(self):
        if self.worker:
            self.worker.cancel()
            self.tick_counter = 0
            self.status = False
            self.update_tick_in_faders(clear_tick=1)
