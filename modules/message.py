"""
isort:skip_file
"""
from resources.midi_message import build_midi_message
from resources.sequencer import (
    apply_mod,
    apply_octave,
    get_actual_values,
    get_first_note_sequencer,
    has_action,
    set_default_values,
    sort_sequencers_by_channel,
)


def build_message(controller, tick_value, tick_counter):
    """
    Constroi a mensagem a ser enviada pela interface
    """
    result_midi_message = []
    sequencers_by_channel = sort_sequencers_by_channel(controller)
    for item in sequencers_by_channel:
        if get_first_note_sequencer(item["sequencers"]):
            raw_message = get_actual_values(
                item["sequencers"], tick_value, tick_counter
            )
            if not has_action(
                raw_message, "Note"
            ):  # go to next loop if there's no Note
                continue
            mod_message = apply_mod(raw_message)
            octaved_message = apply_octave(mod_message)
            clean_message = set_default_values(octaved_message)
            result_midi_message.append(
                build_midi_message(clean_message, item["channel"])
            )

    return result_midi_message
